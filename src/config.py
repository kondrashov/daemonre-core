token = ''

# Address to search jokes, but if another, check bot & website code
quotes_address = 'http://bash.com/?random'
jokes_address = 'https://www.anekdot.ru/random/anekdot/'
nyaddress = 'https://calendar.yoip.ru/calc/days-to-new-year/'
randomcat_address = 'https://thiscatdoesnotexist.com/'
holiday_address = 'https://www.calend.ru/img/export/informer.png'

# If 0 or other, whitelist will be enabled
blacklist = 1

# Demotivator settings
watermarkwrong = '@daemonre'

# PLZ DO NOT SAVE DEFAULT SETTINGS HERE, SET YOURS
bot_ver = '100.0.1.2 Alberto Stable'
build_code = 'OFFICIAL STABLE by nekondrashov (thanks to others)'
bot_name = 'Daemon RE'
group_shortname  = 'daemonre'
admin_username = 'nekondrashov'
admin_funcs = 1

# Paths
modules_dir = 'modules/'
dir_to_pic = 'Images/'
dir_to_txt = 'Dialogs/'
path_to_log = 'Logs/log_{time}.log'
lists_dir = 'Lists/'
lists_files = ['admins.txt', 'commands_blacklist.txt', 'blacklist.txt', 'whitelist.txt']

levels = [0, 100, 1000, 2500, 5000]
level_names = ['newbie', 'typical Daemon RE user', 'Daemon RE active user', 'Daemon RE king', 'Daemon RE god', '...']

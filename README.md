<h1 align="center">
  <img src="assets/logo.jpg" alt="Daemon RE"><br>
</h1>

<p align="center">Daemon RE — это бот для бесед ВКонтакте, написанный на Python 3. Он обучается на вашей переписке , а затем генерирует демотиваторы и пишет сообщения сам!</p>

## Demo

<img src="assets/demo.png">

## Installation

1. Склонируйте репозиторий через `git clone https://codeberg.org/Daemon-RE/daemonre-core.git`
2. Установите библеотеки<br>
на Linux: `pip3 install -r requirements.txt`<br>
на Windows: `pip install -r requirements.txt`<br>
4. Внесите свои настройки в [config.py](src/config.py)
5. Запустите бота через `python3 main.py`
6. (Необязательно) Склонируйте репозиторий модулей `git clone https://codeberg.org/Daemon-RE/daemonre-modules.git` и поместите его в папку modules.

